<?php

namespace Drupal\lametric\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Url;
use Drupal\lametric\Service\LaMetricNotificationInterface;
use Drupal\service\StringTranslationTrait;

/**
 * Trait LaMetricFormTrait.
 */
trait LaMetricFormTrait {

  use CompositeFormElementTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['lametric.settings'];
  }

  /**
   * Creates form structure.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildConnectionForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $form['connection'] = [
      '#type' => !empty(Element::children($form)) ? 'details' : 'container',
      '#title' => $this->t('Connection'),
      '#open' => TRUE,
    ];

    $config = $this->config($this->getEditableConfigNames()[0]);

    $form['connection']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#options' => [
        'app' => $this->t('Application'),
        'local' => $this->t('Local'),
      ],
      '#default_value' => $config->get('local') ? 'local' : 'app',
      // Ignore by checkers because they can not define that actually are
      // callbacks array.
      // @noRector
      // @phpstan-ignore-next-line
      '#pre_render' => array_map(
        fn(string $callback): array => [static::class, $callback],
        static::trustedCallbacks(),
      ),
    ];

    $form['connection']['app']['help']['title'] = [
      '#theme' => 'form_element_label',
      '#title' => $this->t('Repeat the steps described below to find values for these fields:'),
    ];

    $form['connection']['app']['help']['description'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#items' => [
        $this->t('Navigate to the @url tab on the <em>My Apps</em> page.', $this->url('LaMetric Apps', 'applications/list')),
        $this->t('Click on the row of an application that you are interested in.'),
        $this->t('Switch to the next tab after the <em>Store info</em> tab.'),
      ],
    ];

    $form['connection']['app']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Push URL or Local Push URL'),
      '#field_prefix' => LaMetricNotificationInterface::PUSH_URL_PREFIX,
      '#size' => 40,
      '#pattern' => '[a-z0-9]+(|/\d+)',
    ] + $this->example('37054e10e7371825641a2c0a08ce2e42/3');

    $form['connection']['app']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#field_suffix' => LaMetricNotificationInterface::API_TOKEN_SUFFIX,
      '#size' => 95,
      '#pattern' => '[a-zA-Z0-9]+',
    ] + $this->example('YzIxATE1NmYzMzRhODgzMGY2NmM1YWRkNjE3ZmZjqTAwZGQxMTVkMzkyZRM5MDVmNGViMTlnODZkYjUyPTdmNg');

    $form['connection']['local']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#description' => $this->t(
        'See @url where you can found this data.',
        $this->url(
          $this->t('here'),
          'en/latest/guides/first-steps/first-local-notification.html#discover-ip-address',
          'https://lametric-documentation.readthedocs.io/',
        ),
      ),
      '#maxlength' => 15,
      '#size' => 20,
      '#pattern' => '\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}',
    ] + $this->example('192.168.1.8');

    $form['connection']['local']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Copy from the <em>Api Key</em> table column on the @url page.', $this->url('Devices', 'user/devices')),
      '#size' => 70,
      '#pattern' => '[a-z0-9]+',
    ] + $this->example('a10628034bf2a414b8a56663e261d5b7ae46e2864f82754751103da9fe0cf541');

    $this->action(
      $form,
      $config,
      function (
        array &$form,
        Config $config,
        string $type,
        string $field,
      ): void {
        $form['connection'][$type][$field] += [
          '#default_value' => $config->get("types.$type.$field"),
          '#states' => [
            'required' => $form['connection'][$type]['#states']['visible'],
          ],
        ];
      },
      $form_state,
      function (array &$form, Config $config, string $type): void {
        $form['connection'][$type] += [
          '#type' => 'container',
          '#tree' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="type"]' => [
                'value' => $type,
              ],
            ],
          ],
        ];
      }
    );

    return $form;
  }

  /**
   * Describes types of notifications.
   *
   * @param array $element
   *   The form element whose value is being processed.
   */
  public static function preRenderType(array $element): array {
    $element['app']['#description'] = t('Send notification to a device via an application.');
    $element['local']['#description'] = t('Send notification to a device in the same network.');
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderCompositeFormElement', 'preRenderType'];
  }

  /**
   * Gets URL as a list of replacements to make after translation.
   *
   * @param \Drupal\Component\Render\MarkupInterface|array|string $text
   *   The link text for the anchor tag as a translated string or render array.
   *   Strings will be sanitized automatically. If you need to output HTML in
   *   the link text, use a render array or an already sanitized string such as
   *   the output of \Drupal\Component\Utility\Xss::filter() or
   *   \Drupal\Component\Render\FormattableMarkup.
   * @param string $path
   *   The part of the URL without a domain name.
   * @param string $domain
   *   (optional) The domain name from URL. Defaults to
   *   LaMetricNotificationInterface::DOMAIN.
   */
  protected function url(
    MarkupInterface|array|string $text,
    string $path,
    string $domain = LaMetricNotificationInterface::DOMAIN,
  ): array {
    $url = Url::fromUri($domain . $path, [
      'attributes' => [
        'target' => '_blank',
      ],
    ]);

    return ['@url' => Link::fromTextAndUrl($text, $url)->toString()];
  }

  /**
   * Executes some actions based on fields and their sections.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Config\Config $config
   *   The configuration object.
   * @param callable $field_callback
   *   The function for processing each field in the section.
   * @param \Drupal\Core\Form\FormStateInterface|null $form_state
   *   (optional) The current state of the form. Defaults to NULL.
   * @param callable|null $fields_callback
   *   (optional) The function for processing each section. Defaults to NULL.
   */
  protected function action(
    array &$form,
    Config $config,
    callable $field_callback,
    ?FormStateInterface $form_state = NULL,
    ?callable $fields_callback = NULL,
  ): void {
    foreach (array_keys($form['connection']['type']['#options']) as $type) {
      if ($fields_callback) {
        $fields_callback($form, $config, $type);
      }

      foreach (Element::children($form['connection']['local']) as $field) {
        $field_callback($form, $config, $type, $field, $form_state);
      }
    }
  }

  /**
   * Returns renderable example.
   *
   * Gets part of renderable element which provides one from possible values
   * that can be used as filed value.
   *
   * @param mixed $value
   *   The allowed value.
   */
  protected function example(mixed $value): array {
    return [
      '#attributes' => [
        'placeholder' => $this->t('E.g. @example', ['@example' => $value]),
      ],
    ];
  }

}
