<?php

namespace Drupal\lametric\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\service\ConfigFormBase;

/**
 * Configure LaMetric Time settings for this site.
 */
class LaMetricSettingsForm extends ConfigFormBase implements TrustedCallbackInterface {

  use LaMetricFormTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'lametric_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    return parent::buildForm(
      $this->buildConnectionForm($form, $form_state),
      $form_state,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $config = $this->config($this->getEditableConfigNames()[0])
      ->set('local', $form_state->getValue('type') === 'local');

    $this->action(
      $form,
      $config,
      function (
        array &$form,
        Config $config,
        string $type,
        string $field,
        FormStateInterface $form_state,
      ): void {
        $config->set(
          "types.$type.$field",
          $form_state->getValue([$type, $field]),
        );
      },
      $form_state,
    );

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
