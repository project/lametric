<?php

namespace Drupal\lametric\Form;

use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\lametric\Service\LaMetricNotificationTrait;
use Drupal\service\ConfigFactoryTrait;
use Drupal\service\FormBase;
use Drupal\service\MessengerTrait;

/**
 * Class LaMetricTestForm.
 */
class LaMetricTestForm extends FormBase implements TrustedCallbackInterface {

  use ConfigFactoryTrait;
  use ConfigFormBaseTrait;
  use LaMetricFormTrait;
  use LaMetricNotificationTrait;
  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addConfigFactory()
      ->addLaMetricNotification()
      ->addStringTranslation()
      ->addMessenger();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'lametric_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message'),
      '#open' => TRUE,
    ];

    $form['message']['icon'] = [
      '#type' => 'number',
      '#title' => $this->t('Icon identifier'),
      '#description' => $this->t('Select an icon on the @url page.', $this->url('Gallery', 'icons')),
    ] + $this->example(2867);

    $form['message']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#required' => TRUE,
    ] + $this->example($this->t('Hello!'));

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];

    return $this->buildConnectionForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $local = ($type = $form_state->getValue('type')) === 'local';
    $method = $local ? 'sendLocally' : 'sendApplication';

    $response = $this->laMetricNotification()->$method(
      $form_state->getValue([$type, 'url']),
      $form_state->getValue([$type, 'key']),
      $form_state->getValue('text'),
      $form_state->getValue('icon') ?: NULL,
    );

    if ($response instanceof \Exception) {
      $this->messenger()->addError($response->getMessage());
    }
    elseif ($local && !is_numeric($response)) {
      $this->messenger()->addWarning($response);
    }
    else {
      $this->messenger()->addStatus(
        $this->t('The notification has been successfully sent.'),
      );
    }
  }

}
