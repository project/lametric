<?php

namespace Drupal\lametric\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the LaMetric notification service.
 */
trait LaMetricNotificationTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceLaMetricNotification = 'lametric.notification';

  /**
   * Sets the LaMetric notification.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addLaMetricNotification(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the LaMetric notification.
   */
  protected function laMetricNotification(): LaMetricNotificationInterface {
    return $this->getKnownService();
  }

}
