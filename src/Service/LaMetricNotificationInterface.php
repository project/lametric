<?php

namespace Drupal\lametric\Service;

/**
 * Defines the notification service interface.
 */
interface LaMetricNotificationInterface {

  /**
   * The domain name of LaMetric service for developers.
   */
  public const string DOMAIN = 'https://developer.lametric.com/';

  /**
   * The push URL prefix.
   */
  public const string PUSH_URL_PREFIX = self::DOMAIN . 'api/v1/dev/widget/update/com.lametric.';

  /**
   * The API token suffix.
   */
  public const string API_TOKEN_SUFFIX = '==';

  /**
   * Sends a notification to a device based on module settings.
   *
   * @param string $text
   *   The message text.
   * @param int|null $icon
   *   (optional) The icon identifier. Defaults to NULL.
   */
  public function send(string $text, ?int $icon = NULL): mixed;

  /**
   * Sends a notification to a device in the same network.
   *
   * @param string $ip
   *   The IP address.
   * @param string $key
   *   The API key.
   * @param string $text
   *   The message text.
   * @param int|null $icon
   *   (optional) The icon identifier. Defaults to NULL.
   *
   * @return \Exception|int|string
   *   One of the following values:
   *   - The message ID when a notification was sent successfully and the
   *     response structure is correct.
   *   - The response content when a notification was sent successfully but the
   *     response structure is invalid.
   *   - The exception object when a notification was sent unsuccessfully.
   *
   * @see https://lametric-documentation.readthedocs.io/en/latest/guides/first-steps/first-local-notification.html#send-notification
   */
  public function sendLocally(
    string $ip,
    string $key,
    string $text,
    ?int $icon = NULL,
  ): \Exception|int|string;

  /**
   * Sends a notification to a device via an application.
   *
   * @param string $url
   *   The push URL suffix.
   * @param string $token
   *   The access token.
   * @param string $text
   *   The message text.
   * @param int|null $icon
   *   (optional) The icon identifier. Defaults to NULL.
   *
   * @return \Exception|bool
   *   TRUE if a notification was sent successfully otherwise the exception
   *   object.
   *
   * @see https://lametric-documentation.readthedocs.io/en/latest/guides/first-steps/first-lametric-indicator-app.html#push-some-data
   */
  public function sendApplication(
    string $url,
    string $token,
    string $text,
    ?int $icon = NULL,
  ): \Exception|bool;

}
