<?php

namespace Drupal\lametric\Service;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the notification service.
 */
class LaMetricNotification implements LaMetricNotificationInterface {

  /**
   * LaMetricNotification constructor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle HTTP client instance.
   * @param \Drupal\Component\Serialization\SerializationInterface $jsonSerialization
   *   The JSON serialization.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected SerializationInterface $jsonSerialization,
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function send(string $text, ?int $icon = NULL): mixed {
    $config = $this->configFactory->get('lametric.settings');

    if ($config->get('local')) {
      $type = 'local';
      $method = 'sendLocally';
    }
    else {
      $type = 'app';
      $method = 'sendApplication';
    }

    return $this->$method(
      $config->get("types.$type.url"),
      $config->get("types.$type.key"),
      $text,
      $icon,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function sendLocally(
    string $ip,
    string $key,
    string $text,
    ?int $icon = NULL,
  ): \Exception|int|string {
    try {
      $request = $this->httpClient->post(
        sprintf('http://%s:8080/api/v2/device/notifications', $ip),
        [
          RequestOptions::AUTH => ['dev', $key],
          RequestOptions::JSON => ['model' => $this->frames($text, $icon)],
        ],
      );

      $response = $request->getBody()->getContents();
      $content = $this->jsonSerialization->decode($response);

      if (
        isset($content['success']['id']) &&
        is_numeric($content['success']['id'])
      ) {
        return (int) $content['success']['id'];
      }
    }
    catch (\Exception $exception) {
      return $exception;
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function sendApplication(
    string $url,
    string $token,
    string $text,
    ?int $icon = NULL,
  ): \Exception|bool {
    try {
      $this->httpClient->post(self::PUSH_URL_PREFIX . $url, [
        RequestOptions::HEADERS => [
          'X-Access-Token' => $token . self::API_TOKEN_SUFFIX,
          'Cache-Control' => 'no-cache',
        ],
        RequestOptions::JSON => $this->frames($text, $icon),
      ]);
    }
    catch (\Exception $exception) {
      return $exception;
    }

    return TRUE;
  }

  /**
   * Prepares the structure of frames.
   *
   * @param string $text
   *   The message text.
   * @param int|null $icon
   *   (optional) The icon identifier. Defaults to NULL.
   */
  protected function frames(string $text, ?int $icon = NULL): array {
    $frame = ['text' => $text];

    if ($icon !== NULL) {
      $frame['icon'] = $icon;
    }

    return ['frames' => [$frame]];
  }

}
