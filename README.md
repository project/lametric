```
  _          __  __      _        _        _____ _
 | |    __ _|  \/  | ___| |_ _ __(_) ___  |_   _(_)_ __ ___   ___
 | |   / _` | |\/| |/ _ \ __| '__| |/ __|   | | | | '_ ` _ \ / _ \
 | |__| (_| | |  | |  __/ |_| |  | | (__    | | | | | | | | |  __/
 |_____\__,_|_|  |_|\___|\__|_|  |_|\___|   |_| |_|_| |_| |_|\___|
```

# LaMetric Time

Send notification on the LaMetric Time device.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/lametric).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/lametric).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Service](https://www.drupal.org/project/service)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module.
1. Administration > Configuration > Web services > LaMetric Time.
1. Set credentials for connecting to the LaMetric Time device.


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
